<?php
/**
 * template name: Case Study
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site will use a
 * different template.
 *
 * Please see /external/starkers-utilities.php for info on Starkers_Utilities::get_template_parts()
 *
 * @package 	WordPress
 * @subpackage 	Starkers
 * @since 		Starkers 4.0
 */
?>
<?php Starkers_Utilities::get_template_parts( array( 'parts/shared/html-header', 'parts/shared/header' ) ); ?>

<div class="wrp">
    <div class="main_pg"> 
    	
		<?php if ( have_posts() ) while ( have_posts() ) : the_post(); ?>
        <h2><?php the_title(); ?></h2>
        
        
        <?php if (has_post_thumbnail()) { ?>
        <div class="pic"> 
		<?php the_post_thumbnail(); ?>
		</div>
        <?php } ?>
        <div class="case_wrp">
			<?php the_content(); ?> 
       	
        </div>
        <?php endwhile; ?>
        
         
    </div>
    <div class="sidebar">
    	<?php include('sidebar.php'); ?>
    </div>
</div>
<?php Starkers_Utilities::get_template_parts( array( 'parts/shared/footer','parts/shared/html-footer' ) ); ?>