<?php
/**
 * template name: Home Page
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site will use a
 * different template.
 *
 * Please see /external/starkers-utilities.php for info on Starkers_Utilities::get_template_parts()
 *
 * @package 	WordPress
 * @subpackage 	Starkers
 * @since 		Starkers 4.0
 */
?>
<?php Starkers_Utilities::get_template_parts( array( 'parts/shared/html-header', 'parts/shared/header' ) ); ?>

<div class="wrp">
<div class="hm_cnt_bx">
<?php if ( !function_exists('dynamic_sidebar') || !dynamic_sidebar('Home Box 1') ) : ?><?php endif; ?>
</div>
<div class="hm_cnt_bx">
<?php if ( !function_exists('dynamic_sidebar') || !dynamic_sidebar('Home Box 2') ) : ?><?php endif; ?>
</div>
<div class="hm_cnt_bx">
<?php if ( !function_exists('dynamic_sidebar') || !dynamic_sidebar('Home Box 3') ) : ?><?php endif; ?>
</div>
</div>
<?php Starkers_Utilities::get_template_parts( array( 'parts/shared/footer','parts/shared/html-footer' ) ); ?>