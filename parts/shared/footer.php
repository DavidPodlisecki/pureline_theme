<div id="footer">	
	<div class="wrp">
    	<div class="ft_bx">
            <div class="title">
            <h2>Access Quick Links</h2>
            <div class="line">&nbsp;</div>
            </div>
            <div id="ft_nav">
            <?php wp_nav_menu(array('container_class' => 'menu-header', 'theme_location' => 'secondary')); ?>
            </div>
        </div> 
        <div class="ft_bx">
            <div class="title">
            <h2>Case Studies</h2>
            <div class="line">&nbsp;</div>
            </div>
            <div class="ft_caseStudy">
            <?php $loop = new WP_Query(array('post_type' => 'case-study', 'posts_per_page' => 3)); ?>
            <?php while ($loop->have_posts()) : $loop->the_post(); ?>
                  <div class="caseStudy">
					<div class="thumb"><?php the_post_thumbnail(); ?></div>
						<div class="content"><h4><?php the_title (); ?></h4>
                             <?php echo get_post_meta($post->ID, "wpcf-cs-description", $single = true); ?>
                  <a target="_blank" href="<?php echo get_post_meta($post->ID, "wpcf-pdf-link", $single = true); ?> ">Read More</a>
                                       </div>
                                       </div>

									<?php endwhile; ?>
                                                
            </div>
        </div>
        <div class="ft_bx">
            <div class="title">
            <h2>Quick Connect</h2>
            <div class="line">&nbsp;</div>
            </div>
            <div id="ft_form">
            <?php echo do_shortcode('[contact-form-7 id="448" title="Footer Form"]'); ?>


            </div>
        </div>  
        <div class="line">&nbsp;</div>
        <footer style="float:left; width:960px;">
            &copy; <?php echo date("Y"); ?> <?php bloginfo( 'name' ); ?>. All rights reserved.
            <div class="info">
        	<img src="<?php bloginfo('template_directory'); ?>/images/tp_hd_email_icon.png" height="15" width="14" />
            <?php if ( !function_exists('dynamic_sidebar') || !dynamic_sidebar('Header Email') ) : ?><?php endif; ?>
        </div>
            <div class="info">
        	<img src="<?php bloginfo('template_directory'); ?>/images/tp_hd_phone_icon.png" height="20" width="11" />
        	<?php if ( !function_exists('dynamic_sidebar') || !dynamic_sidebar('Header Phone Number') ) : ?><?php endif; ?>
        </div>
        
        </footer>
	</div>
</div>