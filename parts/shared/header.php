<div id="top_header">
	<div class="wrp">
        <div class="info">
        	<img src="<?php bloginfo('template_directory'); ?>/images/tp_hd_phone_icon.png" height="20" width="11" />
        	<?php if ( !function_exists('dynamic_sidebar') || !dynamic_sidebar('Header Phone Number') ) : ?><?php endif; ?>
        </div>
        <div class="info">
        	<img src="<?php bloginfo('template_directory'); ?>/images/tp_hd_email_icon.png" height="15" width="14" />
            <?php if ( !function_exists('dynamic_sidebar') || !dynamic_sidebar('Header Email') ) : ?><?php endif; ?>
        </div>
        <div class="login">
        	<img src="<?php bloginfo('template_directory'); ?>/images/tp_hd_access_icon.png" height="22" width="17"/>
            Access Your Customer Resource Center
          <!-- <a class="btn" href="#">Login</a> -->
            <nav>
	<ul>
		<li id="login">
			<a id="login-trigger" href="#">
				Log in <span>▼</span>
			</a>
			<div id="login-content">
				<form>
					<fieldset id="inputs">
						<input id="username" type="email" name="Email" placeholder="Your email address" required>   
						<input id="password" type="password" name="Password" placeholder="Password" required>
					</fieldset>
					<fieldset id="actions">
						<input type="submit" id="submit" value="Log in">
						<label><input type="checkbox" checked="checked"> Keep me signed in</label>
					</fieldset>
				</form>
			</div>                     
		</li>
		
	</ul>
</nav>
        </div>
    </div>
</div>
<div id="header">
	<div class="wrp">
		<header>
			<a href="<?php echo home_url(); ?>"><h1 class="logo1"><?php bloginfo( 'name' ); ?></h1></a>
			<a href="http://www.thesterlingbridge.com/" target="_blank"><h1 class="logo2">the sterling bridge></h1></a>
            <div class="header_cta">
			<?php bloginfo( 'description' ); ?>
            </div>
            <div class="phone">
			<?php if ( !function_exists('dynamic_sidebar') || !dynamic_sidebar('Header Phone Number') ) : ?><?php endif; ?>
            </div>
</header>
</div>
<div id="main_nav">
	<div class="wrp">
 		<?php wp_nav_menu(array('container_class' => 'menu-header', 'theme_location' => 'primary')); ?>
	</div>
</div>
<?php if(is_front_page()):?>

<?php else:?>
<div class="bread_crumbs">
	<div class="wrp">
	 <?php if(function_exists('bcn_display'))
        {
            bcn_display();
        }?>
     </div>
</div> 
<?php endif;?>

<?php if(is_front_page()):?>
<div id="slider">
	<div class="slider_img">
                    <div class="banner">
                <ul>
                    <li>
                   
                    <img src="http://pureline.baseline21.com/wp-content/uploads/2013/06/wave-sea-water-ocean-splash-1800x2880.jpg" />
                     <div class="banner_bg"><div class="wrp"><h2>Campus & Institutional Buildings<span>Vestibulum id ligula porta felis euismod semper. Donec ullamcorper nulla non metus auctor fringilla</span></h2><a href="#">LEARN MORE</a></div>
					</li>
                    <li>    	
                    
                    <img src="http://pureline.baseline21.com/wp-content/uploads/2013/06/wave-sea-water-ocean-splash-1800x2880.jpg" />
                     <div class="banner_bg"><div class="wrp"><h2>Campus & Institutional Buildings<span>Vestibulum id ligula porta felis euismod semper. Donec ullamcorper nulla non metus auctor fringilla</span></h2><a href="#">LEARN MORE</a></div>
</li>
                    <li>    	<img src="http://pureline.baseline21.com/wp-content/uploads/2013/06/wave-sea-water-ocean-splash-1800x2880.jpg" />
                     <div class="banner_bg"><div class="wrp"><h2>Campus & Institutional Buildings<span>Vestibulum id ligula porta felis euismod semper. Donec ullamcorper nulla non metus auctor fringilla</span></h2><a href="#">LEARN MORE</a></div>
</li>
                </ul>
            </div>
    
    </div>
    <div class="sldr_nav">
    <div class="wrp">
    <img src="<?php bloginfo('template_directory'); ?>/images/Home_nav_placeholder.png" />
    </div>
    </div>
    
</div>
<?php endif;?>
