<?php
/**
 * template name: Home Page test
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site will use a
 * different template.
 *
 * Please see /external/starkers-utilities.php for info on Starkers_Utilities::get_template_parts()
 *
 * @package 	WordPress
 * @subpackage 	Starkers
 * @since 		Starkers 4.0
 */
?>
<?php Starkers_Utilities::get_template_parts( array( 'parts/shared/html-header', 'parts/shared/header' ) ); ?>

<div class="wrp">
<?php       
global $post;
$paged = (get_query_var('paged')) ? get_query_var('paged') : 1;
$args = array(
'category_name' => 'BX',
'numberposts' => '-1',
'order' => 'ASC'
);
$services_posts = get_posts($args);
?>

<div class="span8">

  <ul class="bxslider">

   <?php $count=0;foreach($services_posts as $post) : setup_postdata($post);$count++; ?>
   <?php $bx_large = wp_get_attachment_image_src(get_post_thumbnail_id(), 'bx-large'); ?>

        <li id="slider-<?php echo $count; ?>"><img src="<?php echo $bx_large[0]; ?>" height="<?php echo $bx_large[2]; ?>" width="<?php echo $bx_large[1]; ?>" alt="Image"></li>

   <?php endforeach; ?>

  </ul>

</div>

<div class="span4">

 <div id="bx-pager">

      <div class="row-fluid">

        <?php $count=0;foreach($services_posts as $post) : setup_postdata($post); $count++; ?>
        <?php $bx_small = wp_get_attachment_image_src(get_post_thumbnail_id(), 'bx-small'); ?>

        <a data-slide-index="" href="#slider-<?php echo $count; ?>"><img src="<?php echo $bx_small[0]; ?>" height="<?php echo $bx_small[2]; ?>" width="<?php echo $bx_small[1]; ?>" alt="Image"></a>

        <?php endforeach; ?>

    </div>

    </div>

</div>
</div>
<?php Starkers_Utilities::get_template_parts( array( 'parts/shared/footer','parts/shared/html-footer' ) ); ?>