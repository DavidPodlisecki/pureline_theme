<?php
	/**
	 * Starkers functions and definitions
	 *
	 * For more information on hooks, actions, and filters, see http://codex.wordpress.org/Plugin_API.
	 *
 	 * @package 	WordPress
 	 * @subpackage 	Starkers
 	 * @since 		Starkers 4.0
	 */

	/* ========================================================================================================================
	
	Required external files
	
	======================================================================================================================== */

	require_once( 'external/starkers-utilities.php' );

	/* ========================================================================================================================
	
	Theme specific settings

	Uncomment register_nav_menus to enable a single menu with the title of "Primary Navigation" in your theme
	
	======================================================================================================================== */

	add_theme_support('post-thumbnails');
	
	
	add_action( 'init', 'register_my_menus' );
	function register_my_menus() {
	register_nav_menus(
	array(
'primary' => __( 'Primary Navigation' ),
'secondary' => __( 'Footer Navigation' )
)
);
}

	/* ========================================================================================================================
	
	Actions and Filters
	
	======================================================================================================================== */

	add_action( 'wp_enqueue_scripts', 'starkers_script_enqueuer' );

	add_filter( 'body_class', array( 'Starkers_Utilities', 'add_slug_to_body_class' ) );

	/* ========================================================================================================================
	
	Custom Post Types - include custom post types and taxonimies here e.g.

	e.g. require_once( 'custom-post-types/your-custom-post-type.php' );
	
	======================================================================================================================== */



	/* ========================================================================================================================
	
	Scripts
	
	======================================================================================================================== */

	/**
	 * Add scripts via wp_head()
	 *
	 * @return void
	 * @author Keir Whitaker
	 */

	function starkers_script_enqueuer() {
		wp_register_script( 'site', get_template_directory_uri().'/js/site.js', array( 'jquery' ) );
		wp_enqueue_script( 'site' );

		wp_register_style( 'screen', get_stylesheet_directory_uri().'/style.css', '', '', 'screen' );
        wp_enqueue_style( 'screen' );
	}
include( get_template_directory() . '/resize.php' );	

	/* ========================================================================================================================
	
	Comments
	
	======================================================================================================================== */

	/**
	 * Custom callback for outputting comments 
	 *
	 * @return void
	 * @author Keir Whitaker
	 */
	function starkers_comment( $comment, $args, $depth ) {
		$GLOBALS['comment'] = $comment; 
		?>
		<?php if ( $comment->comment_approved == '1' ): ?>	
		<li>
			<article id="comment-<?php comment_ID() ?>">
				<?php echo get_avatar( $comment ); ?>
				<h4><?php comment_author_link() ?></h4>
				<time><a href="#comment-<?php comment_ID() ?>" pubdate><?php comment_date() ?> at <?php comment_time() ?></a></time>
				<?php comment_text() ?>
			</article>
		<?php endif;
	}
	
	if ( function_exists('register_sidebar') )
    register_sidebar( array(
   'name' => __( 'Header Phone Number'),
   'id' => 'header_phone',
   'description' => __( 'Header Phone Number', 'twentyeleven' ),
   'before_widget' => '<aside id="%1$s" class="widget %2$s">',
   'after_widget' => "</aside>",

   ) );
   if ( function_exists('register_sidebar') )
    register_sidebar( array(
   'name' => __( 'Header Email'),
   'id' => 'header_email',
   'description' => __( 'Header Email', 'twentyeleven' ),
   'before_widget' => '<aside id="%1$s" class="widget %2$s">',
   'after_widget' => "</aside>",

   ) );
   if ( function_exists('register_sidebar') )
    register_sidebar( array(
   'name' => __( 'Home Call To Action'),
   'id' => 'home_cta',
   'description' => __( 'Hompage Middle Call To Action', 'twentyeleven' ),
   'before_widget' => '<aside id="%1$s" class="widget %2$s">',
   'after_widget' => "</aside>",

   ) );
   if ( function_exists('register_sidebar') )
    register_sidebar( array(
   'name' => __( 'Home Box 1'),
   'id' => 'home_bx1',
   'description' => __( 'First Hompage Content Box', 'twentyeleven' ),
   'before_widget' => '<aside id="%1$s" class="widget %2$s">',
   'after_widget' => "</aside>",

   ) );

if ( function_exists('register_sidebar') )
    register_sidebar( array(
   'name' => __( 'Home Box 2'),
   'id' => 'home_bx2',
   'description' => __( 'Second Hompage Content Box', 'twentyeleven' ),
   'before_widget' => '<aside id="%1$s" class="widget %2$s">',
   'after_widget' => "</aside>",

   ) );

if ( function_exists('register_sidebar') )
    register_sidebar( array(
   'name' => __( 'Home Box 3'),
   'id' => 'home_bx3',
   'description' => __( 'Third Hompage Content Box', 'twentyeleven' ),
   'before_widget' => '<aside id="%1$s" class="widget %2$s">',
   'after_widget' => "</aside>",

   ) );

if ( function_exists('register_sidebar') )
    register_sidebar( array(
   'name' => __( 'Sidebar News'),
   'id' => 'sidebar_news',
   'description' => __( 'Sidebar news feed', 'twentyeleven' ),
   'before_widget' => '<aside id="%1$s" class="widget %2$s">',
   'after_widget' => "</aside>",

   ) );
   if ( function_exists('register_sidebar') )
    register_sidebar( array(
   'name' => __( 'Sidebar Testimonial'),
   'id' => 'sidebar_testimonial',
   'description' => __( 'Sidebar Testimonail', 'twentyeleven' ),
   'before_widget' => '<aside id="%1$s" class="widget %2$s"><h2>Testimonials</h2>',
   'after_widget' => "</aside>",

   ) );
   
  if (class_exists('MultiPostThumbnails')) {
        new MultiPostThumbnails(
            array(
                'label' => 'slide Thumbnail',
                'id' => 'slide_thumb',
                'post_type' => 'Imageslides'
            )
        );
    }
